package com.example.jenniferjones.ubercodechallenge.ui.gallery;

import android.util.Log;

import com.example.jenniferjones.ubercodechallenge.data.model.PhotoPage;
import com.example.jenniferjones.ubercodechallenge.domain.GetPhotos;
import com.example.jenniferjones.ubercodechallenge.domain.common.DomainCallback;

public class GalleryPresenter implements GalleryContract.UserActionsListener {
    private static final String TAG = GalleryPresenter.class.getSimpleName();
    private static final int FLICKR_FIRST_PAGE = 1;
    private GalleryContract.View mView;
    private GetPhotos mGetPhotos;

    private String mSearchText;
    private int mPageNumber = FLICKR_FIRST_PAGE;

    private boolean isLoading = false;
    private PhotoPage mCurrentPage;

    public GalleryPresenter(GalleryContract.View view) {
        mView = view;
        mGetPhotos = new GetPhotos();

        mView.showPhotosEmpty(true, "Search for something");
    }

    @Override
    public void onSearchClicked() {
        mView.showSearch();
    }

    @Override
    public void onCloseSearch() {
        mView.hideSearch();
    }

    @Override
    public void onSearchSubmitted() {
        mSearchText = mView.getSearchBoxText();

        //Reset page number for new searches
        mPageNumber = FLICKR_FIRST_PAGE;

        mView.setTitle(mSearchText);
        mView.hideSearch();
        mView.resetGallery();
        mView.showPhotosEmpty(false, null);

        requestPhotos();
    }


    @Override
    public void onLoadMore() {
        if (!isLoading) {
            if (mCurrentPage == null || mCurrentPage.getPages() > mPageNumber) {
                mPageNumber++;
                requestPhotos();
            }
        }
    }

    private void requestPhotos() {
        isLoading = true;
        mView.showLoading(isLoading);

        mGetPhotos.getPhotosForTextByPage(
                new DomainCallback<PhotoPage>() {
                    @Override
                    public void onSuccess(PhotoPage photoPage) {
                        Log.d(TAG, "onSuccess: " + photoPage);

                        isLoading = false;

                        mCurrentPage = photoPage;

                        mView.showLoading(isLoading);

                        //There aren't any photos for search term
                        if (mCurrentPage == null || mCurrentPage.getTotal() == 0) {
                            mView.showPhotosEmpty(true, "We couldn't find any photos matching \"" + mSearchText + "\"");
                        } else {
                            mView.addPhotos(mCurrentPage.getPhotos());
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        isLoading = false;
                        mView.showLoading(isLoading);
                        mView.showToast(throwable.getMessage());
                    }
                }, mSearchText, mPageNumber
        );
    }

}