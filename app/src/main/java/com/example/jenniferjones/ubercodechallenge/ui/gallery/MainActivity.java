package com.example.jenniferjones.ubercodechallenge.ui.gallery;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jenniferjones.ubercodechallenge.R;
import com.example.jenniferjones.ubercodechallenge.data.model.Photo;
import com.example.jenniferjones.ubercodechallenge.databinding.ActivityMainBinding;
import com.example.jenniferjones.ubercodechallenge.ui.common.SingleItemAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GalleryContract.View {
    public static final String TAG = MainActivity.class.getSimpleName();

    private GalleryPresenter mPresenter;
    private SingleItemAdapter<Photo> mPhotosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mPresenter = new GalleryPresenter(this);

        mPhotosAdapter = new SingleItemAdapter<>(R.layout.item_gallery_photo, new ArrayList<Photo>());

        binding.setActionListener(mPresenter);
        binding.recyclerGallery.setAdapter(mPhotosAdapter);
        binding.recyclerGallery.setLayoutManager(new GridLayoutManager(this, 3));
        binding.recyclerGallery.addOnScrollListener(new EndOfScrollListener());

        setSupportActionBar(binding.toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            mPresenter.onSearchClicked();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void addPhotos(List<Photo> listings) {
        mPhotosAdapter.addItems(listings);
    }

    @Override
    public void showPhotosEmpty(boolean isVisible, String message) {
        TextView textView = (TextView)findViewById(R.id.empty_state_view);

        textView.setText(message);
        textView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean isLoading) {
        findViewById(R.id.loading_view).setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showSearch() {
        View searchMenuItemView = findViewById(R.id.action_search);

        int location[] = new int[2];
        searchMenuItemView.getLocationOnScreen(location);

        int cx = location[0] + searchMenuItemView.getWidth() / 2;
        int cy = searchMenuItemView.getHeight() / 2;

        // previously invisible view
        View searchView = findViewById(R.id.search_view);


        // get the final radius for the clipping circle
        float finalRadius = (float) Math.hypot(searchView.getWidth(), searchView.getHeight());

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(searchView, cx, cy, 0, finalRadius);

        // make the view visible and start the animation
        searchView.setVisibility(View.VISIBLE);
        searchView.setClickable(true);

        anim.start();

        findViewById(R.id.edt_search_text).requestFocus();

    }

    @Override
    public void hideSearch() {
        // previously visible view
        final View searchView = findViewById(R.id.search_view);


        View searchMenuItemView = findViewById(R.id.action_search);

        int location[] = new int[2];
        searchMenuItemView.getLocationOnScreen(location);

        int cx = location[0] + searchMenuItemView.getWidth() / 2;
        int cy = searchMenuItemView.getHeight() / 2;
        // get the initial radius for the clipping circle
        float initialRadius = (float) Math.hypot(searchView.getWidth(), searchView.getHeight());

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(searchView, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                searchView.setVisibility(View.INVISIBLE);
                searchView.setClickable(false);
            }
        });


        ((EditText)findViewById(R.id.edt_search_text)).onEditorAction(EditorInfo.IME_ACTION_DONE);

        // start the animation
        anim.start();

    }

    @Override
    public String getSearchBoxText() {
        EditText searchText = (EditText)findViewById(R.id.edt_search_text);

        return searchText.getText().toString();
    }

    @Override
    public void resetGallery() {
        mPhotosAdapter.clearAllItems();
    }

    @Override
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }


    @BindingAdapter("bind:photo")
    public static void loadImage(ImageView view, Photo photo) {
        String url = "http://farm"+photo.getFarm()+".static.flickr.com/"+photo.getServer()+"/"+photo.getId()+"_"+photo.getSecret()+".jpg";

        Picasso.with(view.getContext()).load(url).placeholder(R.color.colorPrimary).into(view);
    }

    private class EndOfScrollListener extends RecyclerView.OnScrollListener {
        private static final int OFF_SCREEN_THRESHOLD = 6;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            //Assuming GridLayoutManager. Don't do this.
            int lastVisibleItemPos = ((GridLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            int totalNumberItems = recyclerView.getAdapter().getItemCount();

            if((totalNumberItems - lastVisibleItemPos) <= OFF_SCREEN_THRESHOLD){
                mPresenter.onLoadMore();
            }

            super.onScrolled(recyclerView, dx, dy);
        }
    }

}
