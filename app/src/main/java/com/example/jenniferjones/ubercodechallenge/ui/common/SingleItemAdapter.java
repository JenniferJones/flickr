package com.example.jenniferjones.ubercodechallenge.ui.common;

import java.util.List;

public class SingleItemAdapter<T> extends DataBoundBaseAdapter {
    private final int mLayoutId;
    private List<T> mItems;

    public SingleItemAdapter(int layoutId, List<T> items){
        mLayoutId = layoutId;
        mItems = items;
    }

    @Override
    protected T getObjectForPosition(int position) {
        return mItems.get(position);
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return mLayoutId;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItems(final List<T> newItems) {
        mItems.addAll(newItems);
        notifyDataSetChanged();
    }

    public void clearAllItems(){
        int count = mItems.size();
        mItems.clear();
        notifyItemRangeRemoved(0, count);
    }
}
