package com.example.jenniferjones.ubercodechallenge.domain;

import com.example.jenniferjones.ubercodechallenge.data.api.FlickrApi;
import com.example.jenniferjones.ubercodechallenge.data.api.response.FlickrPhotosSearchResponse;
import com.example.jenniferjones.ubercodechallenge.data.model.PhotoPage;
import com.example.jenniferjones.ubercodechallenge.domain.common.ApiError;
import com.example.jenniferjones.ubercodechallenge.domain.common.DomainCallback;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GetPhotos {
    private static final String TAG = GetPhotos.class.getSimpleName();
    private final FlickrApi mFlickrApi;

    //Would inject FlickerApi here
    public GetPhotos() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.flickr.com/")
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
        mFlickrApi = retrofit.create(FlickrApi.class);
    }

    public void getPhotosForTextByPage(final DomainCallback<PhotoPage> callback, String searchText, int page) {
        mFlickrApi.getPhotosForTextByPage(searchText, page)
                .enqueue(new Callback<FlickrPhotosSearchResponse>() {
                    @Override
                    public void onResponse(Call<FlickrPhotosSearchResponse> call, Response<FlickrPhotosSearchResponse> response) {
                            if(response.isSuccessful()) {
                                callback.onSuccess(response.body().getPhotoPage());
                            }
                            else{
                                callback.onError(new ApiError());
                            }
                    }

                    @Override
                    public void onFailure(Call<FlickrPhotosSearchResponse> call, Throwable t) {
                        callback.onError(t);
                    }
                });
    }
}
