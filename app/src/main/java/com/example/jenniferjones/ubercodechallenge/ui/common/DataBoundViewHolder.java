package com.example.jenniferjones.ubercodechallenge.ui.common;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

import com.example.jenniferjones.ubercodechallenge.BR;

public class DataBoundViewHolder extends RecyclerView.ViewHolder {
    private final ViewDataBinding binding;

    public DataBoundViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Object data) {
        binding.setVariable(BR.data, data);
        binding.executePendingBindings();
    }
}
