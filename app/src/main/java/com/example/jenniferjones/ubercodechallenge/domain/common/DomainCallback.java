package com.example.jenniferjones.ubercodechallenge.domain.common;

public interface DomainCallback<T> {

    void onSuccess(T t);

    void onError(Throwable throwable);

}
