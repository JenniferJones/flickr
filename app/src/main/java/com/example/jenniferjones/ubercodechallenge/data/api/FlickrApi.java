package com.example.jenniferjones.ubercodechallenge.data.api;

import com.example.jenniferjones.ubercodechallenge.data.api.response.FlickrPhotosSearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlickrApi {
    @GET("services/rest/?method=flickr.photos.search&" +
            "api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1")
    //per page defaults to 100
    Call<FlickrPhotosSearchResponse> getPhotosForTextByPage(@Query("text") String text, @Query("page") int page);

}