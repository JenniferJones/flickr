package com.example.jenniferjones.ubercodechallenge.ui.gallery;

import com.example.jenniferjones.ubercodechallenge.data.model.Photo;

import java.util.List;

public interface GalleryContract {
    interface View {
        void addPhotos(List<Photo> listings);

        void showPhotosEmpty(boolean isVisible, String message);

        void showToast(String message);

        void showLoading(boolean isLoading);

        void showSearch();

        void hideSearch();

        String getSearchBoxText();

        void resetGallery();

        void setTitle(String title);
    }

    interface UserActionsListener {
        void onSearchClicked();

        void onCloseSearch();

        void onSearchSubmitted();

        void onLoadMore();
    }
}
