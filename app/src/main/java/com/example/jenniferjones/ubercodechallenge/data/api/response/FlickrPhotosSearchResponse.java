package com.example.jenniferjones.ubercodechallenge.data.api.response;

import com.example.jenniferjones.ubercodechallenge.data.model.PhotoPage;
import com.google.gson.annotations.SerializedName;

public class FlickrPhotosSearchResponse {

    @SerializedName("photos")
    private PhotoPage photoPage;

    public PhotoPage getPhotoPage() {
        return photoPage;
    }

    public void setPhotoPage(PhotoPage photoPage) {
        this.photoPage = photoPage;
    }

    public FlickrPhotosSearchResponse(PhotoPage photoPage) {
        this.photoPage = photoPage;
    }
}
