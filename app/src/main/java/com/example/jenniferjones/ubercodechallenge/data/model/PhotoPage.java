package com.example.jenniferjones.ubercodechallenge.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhotoPage {
    @SerializedName("page")
    int mPage;
    @SerializedName("pages")
    int mPages;
    @SerializedName("perpage")
    int mPerPage;
    @SerializedName("total")
    int mTotal;
    @SerializedName("photo")
    List<Photo> mPhotos;

    public PhotoPage(int page, int pages, int perPage, int total, List<Photo> photos) {
        mPage = page;
        mPages = pages;
        mPerPage = perPage;
        mTotal = total;
        mPhotos = photos;
    }

    public int getPage() {
        return mPage;
    }

    public void setPage(int page) {
        mPage = page;
    }

    public int getPages() {
        return mPages;
    }

    public void setPages(int pages) {
        mPages = pages;
    }

    public int getPerPage() {
        return mPerPage;
    }

    public void setPerPage(int perPage) {
        mPerPage = perPage;
    }

    public int getTotal() {
        return mTotal;
    }

    public void setTotal(int total) {
        mTotal = total;
    }

    public List<Photo> getPhotos() {
        return mPhotos;
    }

    public void setPhotos(List<Photo> photos) {
        mPhotos = photos;
    }

    @Override
    public String toString() {
        return "PhotoPage{" +
                "mPage=" + mPage +
                ", mPages=" + mPages +
                ", mPerPage=" + mPerPage +
                ", mTotal=" + mTotal +
                ", mPhotos=" + mPhotos +
                '}';
    }
}
